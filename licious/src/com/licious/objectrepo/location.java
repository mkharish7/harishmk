package com.licious.objectrepo;

import java.util.Properties;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.licious.generc.FileDataUtiles;
import com.licious.generc.WebdrivercommonUtils;


public class location {
	
	
WebdrivercommonUtils wlib = new WebdrivercommonUtils();
  
	@FindBy(id="location_pop")
	private WebElement selectloc;

	
	
	public void location(String loc) throws Throwable {
		wlib.waitForPageToLoad();
		selectloc.sendKeys(loc);
		
		//selectloc.sendKeys(Keys.ENTER);
		
	}
	

}
